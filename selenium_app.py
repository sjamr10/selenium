#!/usr/bin/env python3

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
import time
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import json
from sqlalchemy import create_engine
from sqlalchemy import Column, DateTime, Integer, Numeric, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exists
import datetime
from yattag import Doc


db_string = "postgres://sergiomiranda:@localhost:5432/postgres"

db = create_engine(db_string)
base = declarative_base()


class Ad(base):
    __tablename__ = "ads"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    publication_time = Column(DateTime(timezone=True))
    price = Column(Numeric(precision=14, scale=2))
    link = Column(String, unique=True)
    commune = Column(String)
    image = Column(String)


Session = sessionmaker(db)
session = Session()



def create_table():
    table_name = "ads"
    # If table does not exist, create it.
    if not db.dialect.has_table(db, table_name):
        print(table_name + " table does not exist.\nCreating...")
        base.metadata.create_all(db)
        print(table_name + " table created successfully.")
    else:
        print(table_name + " already exists.")

    print()

# # Create
# ad = Ad(title="asdsadsadsa", publication_time="2017-05-01 19:00:54-03", price="2000", link="url")
# session.add(ad)
# session.commit()


# # Read
# ads = session.query(Ad)
# for ad in ads:
#     print(ad.id)
#     print(ad.title)
#     print(ad.publication_time)
#     print(ad.price)
#     print(ad.link)

#
# # Update
# doctor_strange.title = "Some2016Film"
# session.commit()
#
# # Delete
# session.delete(doctor_strange)
# session.commit()


def delete_ads():
    # Delete every row
    ads = session.query(Ad)
    for ad in ads:
        session.delete(ad)
        session.commit()


def exists_link(link):
    exists_ad = session.query(exists().where(Ad.link==link)).scalar()
    if exists_ad:
        print("Ad already on database")
    else:
        print("Ad not found on database")
    return exists_ad


def save_to_db(ad):
    print("Saving ad to database...")
    session.add(ad)
    session.commit()
    print("Ad saved successfully")


def get_ads():
    expected_title = "Arriendo en Región Metropolitana | yapo.cl"

    driver = webdriver.PhantomJS()
    driver.set_page_load_timeout(10)

    new_ads = list()

    urls = [
            "http://www.yapo.cl/region_metropolitana/arrendar?ca=15_s&l=0&w=1&cmn=313&cmn=315&cmn=319&cmn=320&cmn=323&cmn=330&cmn=338&cmn=340&cmn=343&ret=7&ps=2&pe=5",
        ]

    for url in urls:  
        try:
            driver.get(url)
        except TimeoutException as ex:
            print("It took more than 10 seconds for page to load")
            print()
        finally:
            title = driver.title
            print(title)
            print()
            if expected_title in title:
                ads = driver.find_elements_by_class_name("ad")
                for ad in ads:
                    title = ad.find_element_by_class_name("title").text
                    date = ad.find_element_by_class_name("date").text
                    hour = ad.find_element_by_class_name("hour").text
                    price = ad.find_element_by_class_name("price").text[2:].replace(".", "")
                    link = ad.find_element_by_class_name("redirect-to-url").get_attribute("href")
                    link = link[:link.index('.htm?ca')]
                    commune = ad.find_element_by_class_name("commune").text
                    image = ad.find_element_by_class_name("image").get_attribute("src")
                    image = image.replace("thumbs", "images")
                    image = image.replace("imagesli", "images")

                    today = datetime.date.today()
                    # print(today)
                    if date == "Hoy":
                        date = today.strftime('%Y-%m-%d')
                    elif date == "Ayer":
                        date = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
                    else:
                        break
                    publication_time = date + " " + hour + ":00-03"
                    print()
                    new_ad = Ad(title=title, publication_time=publication_time, price=price, link=link, commune=commune, image=image)
                    if not exists_link(link):
                        print()
                        save_to_db(new_ad)
                        new_ads.append(new_ad)
                    else:
                        break
                    print()
                    print("Title: " + new_ad.title)
                    print("Date: " + date)
                    print("Hour: " + hour)
                    print("Price: $" + '{0:,}'.format(round(new_ad.price)))
                    print("Link: " + new_ad.link)
                    print("Commune: " + new_ad.commune)
                    print("Image: " + new_ad.image)
                    print()
                # time.sleep(1)
                print()
            else:
                print("Incorrect title")
    if len(new_ads) > 0:
        send_email(new_ads)
    driver.quit()

def send_email(new_ads):
    fromaddr = "sm651164@gmail.com"
    toaddrs  = "sjamr10@gmail.com"

    def create_html():
        doc, tag, text = Doc().tagtext()

        with tag('h1'):
            text('Nuevos avisos de piezas:')

        for ad in new_ads:
            with tag('h2'):
                text(str(ad.title))
            with tag('h3'):
                text('$' + '{0:,}'.format(round(ad.price)))
            with tag('h3'):
                text(ad.link)
            with tag('div', id='photo-container'):
                doc.stag('img', src=ad.image, klass="photo")
            with tag('br'):
                text('------------------------------------------------------------')

        print(doc.getvalue())
        return doc.getvalue()


    content = create_html()

    msg = MIMEText(content.encode('utf-8'), 'html', 'utf-8')
    msg['From'] = fromaddr
    msg['To'] = toaddrs
    msg['Subject'] = Header('Aviso nuevo', 'utf-8')

    username = "sm651164@gmail.com"
    password = "usQ8DGeeL637Y34f"
    server = smtplib.SMTP("smtp.gmail.com:587")
    server.ehlo()
    server.starttls()
    server.login(username, password)
    print()
    print("Sending email...")
    server.sendmail(fromaddr, toaddrs, msg.as_string())
    print("Email sent successfully")
    server.quit()



if __name__ == '__main__':
    while True:
        # delete_ads()
        # create_table()
        get_ads()
        time.sleep(240)
        print()
